import os
from fastapi.templating import Jinja2Templates

def configure_templates():
    dev_mode = True

    folder = os.path.dirname(__file__)
    template_folder = os.path.join(folder, '..' ,'templates')
    template_folder = os.path.abspath(template_folder)

    #fastapi_chameleon.global_init(template_folder, auto_reload=dev_mode)
    templates = Jinja2Templates(directory=template_folder)
    print("template_folder: ", template_folder)
    return templates