from fastapi import APIRouter
router = APIRouter()
from fastapi import FastAPI, Request
from viewmodels.packages.details_viewmodel import DetailsViewModel

from utils.configure_templates import configure_templates
templates = configure_templates()

from pprint import pprint

@router.get('/project/{package_name}')
async def details(package_name: str, request: Request):
    print("package_name", package_name)
    vm = DetailsViewModel(package_name, request)
    await vm.load()
    pprint(vm.__dict__)
    if vm.__dict__["package"] != None:
        return templates.TemplateResponse("packages/details.html", vm.__dict__ )