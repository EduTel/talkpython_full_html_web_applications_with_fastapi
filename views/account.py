from fastapi import APIRouter
#from starlette.requests import Request
from fastapi import FastAPI
from starlette.requests import Request

from starlette import status
router = APIRouter()
import fastapi

import asyncio

from infrastructure import cookie_auth
from services import user_service
from viewmodels.account.account_viewmodel import AccountViewModel
from viewmodels.account.login_viewmodel import LoginViewModel
from viewmodels.account.register_viewmodel import RegisterViewModel

from utils.configure_templates import configure_templates
templates = configure_templates()

@router.get('/account', include_in_schema=False)
async def index(request: Request):
    vm = AccountViewModel(request)
    await vm.load()
    return templates.TemplateResponse("account/index.html", vm.__dict__ )

@router.get('/account/register', include_in_schema=False)
def register(request: Request):
    vm = RegisterViewModel(request)
    return templates.TemplateResponse("account/register.html", vm.__dict__ )

@router.post('/account/register', include_in_schema=False)
async def register(request: Request):
    vm = RegisterViewModel(request)
    await vm.load()

    if vm.error:
        return vm.t__dict__

    # Create the account
    account = await user_service.create_account(vm.name, vm.email, vm.password)

    # Login user
    response = fastapi.responses.RedirectResponse(url='/account', status_code=status.HTTP_302_FOUND)
    cookie_auth.set_auth(response, account.id)

    return response
    #return templates.TemplateResponse("account/register.html", response) 

@router.get('/account/login', include_in_schema=False)
def login_get(request: Request):
    vm = LoginViewModel(request)
    return templates.TemplateResponse("account/login.html", vm.__dict__ )

@router.post('/account/login', include_in_schema=False)
async def login_post(request: Request):
    vm = LoginViewModel(request)
    await vm.load()

    if vm.error:
        return vm.__dict__

    user = await user_service.login_user(vm.email, vm.password)
    if not user:
        from pprint import pprint
        vm.error = "The account does not exist or the password is wrong."
        #vm.request = ""
        pprint(vm.__dict__)
        print(type(vm.__dict__))
        print(vm.request.__dict__)
        await asyncio.sleep(10)
        #return {}
        #return vm.__dict__
        return templates.TemplateResponse("account/login.html", vm.__dict__ )

    resp = fastapi.responses.RedirectResponse('/account', status_code=status.HTTP_302_FOUND)
    cookie_auth.set_auth(resp, user.id)
    return resp

@router.get('/account/logout', include_in_schema=False)
def logout(request: Request):
    response = fastapi.responses.RedirectResponse(url='/', status_code=status.HTTP_302_FOUND)
    cookie_auth.logout(response)

    return response
