from fastapi import APIRouter

#from fastapi_chameleon import template
from fastapi import FastAPI, Request

router = APIRouter()

from fastapi.responses import HTMLResponse

from utils.configure_templates import configure_templates
templates = configure_templates()

from pprint import pprint
from viewmodels.home.indexviewmodel import IndexViewModel
from viewmodels.shared.viewmodel import ViewModelBase
#@template('home/index.pt')
@router.get('/', response_class=HTMLResponse)
async def index(request: Request):
    #print("index")
    #return { "user_name": user }
    vm = IndexViewModel(request)
    await vm.load()
    pprint(vm.__dict__)
    return templates.TemplateResponse("home/index.html", vm.__dict__ )

@router.get('/about')
def about(request: Request):
    vm =  ViewModelBase(request)
    return {}