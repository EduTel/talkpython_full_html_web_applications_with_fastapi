import os
import uvicorn
#import fastapi_chameleon
#from fastapi_chameleon import template

from fastapi import FastAPI

app = FastAPI()

from views import home
from views import account
from views import packages

from pathlib import Path
from data import db_session

from fastapi.staticfiles import StaticFiles

from utils.configure_templates import configure_templates

def main():
    configurate(dev_mode=True)
    uvicorn.run("index:app", host='127.0.0.1', port=8000, debug=True)
    #uvicorn.run("index:app", host='127.0.0.1', port=8000, debug=True, reload=True)

import fastapi_chameleon
def configurate(dev_mode: bool):
    #configure_templates(dev_mode)
    fastapi_chameleon.global_init('templates', auto_reload=dev_mode)
    configure_static()
    configure_router()
    configure_db(dev_mode)


def configure_db(dev_mode: bool):
    print("**********************configure_db: ")
    file = (Path(__file__).parent / 'db' / 'pypi.sqlite').absolute()
    db_session.global_init(file.as_posix())


def configure_static():
    app.mount("/static", StaticFiles(directory="static"), name="static")


def configure_router():
    app.include_router(home.router)
    app.include_router(account.router)
    app.include_router(packages.router)


if __name__ == '__main__':
    main()
else:
    configurate(dev_mode=True)